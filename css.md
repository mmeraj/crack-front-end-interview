# What is CSS selector
We use CSS selector to get a DOM element, so that we can apply some CSS over them or we can add some action on them.

e.g. we use # for id, dot for class, tag-name for tag etc.
# What are CSS selector priorities

basically more specific element will have more priority then lesser one which is also called as specificity rule.
e.g. ID is more specific then class or first-of-type is more specific then nth-of-type so previous will have more priority to 
get css applied then later one.

# What is media-queries. What is responsive design and how will you use media-queries for responsive design
media-queries helps us to apply css for specific type of screen/device/resolution. using this we can create responsive design as well so 
our application will behave differently on different size of screen.
e.g. @media only screen and (max-width: 600px and min-width: 320px)
# What are grid structure in Tweeter Bootstrap
Bootstrap's grid system divides whole screen width in 12 columns. we can group the columns together to create wider columns for different screen sizes

e.g. xs (for phones - screens less than 768px wide), sm (for tablets - screens equal to or greater than 768px wide), 
md (for small laptops - screens equal to or greater than 992px wide), lg (larger screen)
# What is BOX module 
box-module says about in our content box first margin do comes, then border, then padding, then content 
# What is flex layout
Flex layout is recently added in CSS3 and most of the time it makes our life very easy while creating complex layout which was previously could 
only be done via javascript, display table or calc. 

# What is difference between position absolute, relative, static, fixed

static: it takes place where the element should be present. it is default position.
absolute: it takes place from adjacent non-static parent.
relative: it takes place from adjacent parent
fixed: it takes place from view-port(visible screen)

# what is difference between dispaly inline, inline-block, block 
block: it is multiline element
inline: it is single line element and it honers line-hight etc.
inline-block: it is single line element and it doesn't honers line-hight.
# what does border-box do?
if box-sizing is set to `border-box`, width of item will be calculated by including border, padding and content.
